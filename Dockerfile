FROM rust:1.58 as builder

RUN rustup target add x86_64-unknown-linux-gnu
WORKDIR /app
COPY . .
RUN cargo build  --target x86_64-unknown-linux-gnu


FROM debian:buster-slim
WORKDIR /app
COPY --from=builder /app/target/x86_64-unknown-linux-gnu/debug/firu /usr/local/bin/firu
CMD ["firu"]
