use git2::{DescribeOptions, Repository};
use std::fs;
use std::path::PathBuf;
use tera::{Context, Tera};

const TEMPLATE_PATH: &str = "templates/**/*";
const STATIC_PATH: &str = "static/";
const OUTPUT_PATH: &str = ".output/";
const OUTPUT_FILE: &str = ".output/index.html";
const DATA_FILE: &str = "data.json";

fn main() {
    let output_directory = PathBuf::from(OUTPUT_PATH);
    let output_file = PathBuf::from(OUTPUT_FILE);
    let data_file = PathBuf::from(DATA_FILE);
    let static_directory = PathBuf::from(STATIC_PATH);

    let tera = match Tera::parse(TEMPLATE_PATH) {
        Ok(tera) => tera,
        Err(error) => {
            eprintln!("{}", error);
            std::process::exit(1);
        }
    };

    // Remove the output directory
    if output_directory.exists() {
        if let Err(error) = fs::remove_dir_all(&output_directory) {
            eprintln!("{:?}: {}", &output_directory, error);
            std::process::exit(1);
        }
    }

    // Copy contents of static folder to output
    if static_directory.exists() {
        if let Err(error) = dircpy::copy_dir(&static_directory, &output_directory) {
            eprintln!("{:?}: {}", &output_file, error);
            std::process::exit(1);
        }
    }

    // Create output directory if it doesn't exist
    if !output_directory.exists() {
        if let Err(error) = fs::create_dir(&output_directory) {
            eprintln!("{:?}: {}", &output_directory, error);
            std::process::exit(1);
        }
    }

    if !data_file.exists() {
        eprintln!("Data file {:?} doesn't exist", &data_file);
        std::process::exit(1);
    }

    let json_string = match fs::read_to_string(data_file) {
        Ok(json_string) => json_string,
        Err(error) => {
            eprintln!("{}", error);
            std::process::exit(1);
        }
    };

    let data = match serde_json::from_str(&json_string) {
        Ok(data) => data,
        Err(error) => {
            eprintln!("{}", error);
            std::process::exit(1);
        }
    };

    let mut ctx = match Context::from_value(data) {
        Ok(ctx) => ctx,
        Err(error) => {
            eprintln!("{}", error);
            std::process::exit(1);
        }
    };

    // TODO: Move this stuff to separate functions for cleaner code
    if let Ok(repository) = Repository::open(".") {
        if let Ok(head) = repository.head() {
            if let Ok(head_commit) = head.peel_to_commit() {
                if let Ok(short_id_buf) = head_commit.as_object().short_id() {
                    if let Ok(commit_hash) = std::str::from_utf8(&short_id_buf) {
                        ctx.insert("GIT_COMMIT", &commit_hash);
                    }
                }
            }
        }

        if let Ok(repository_description) = repository.describe(&DescribeOptions::new()) {
            if let Ok(git_tag) = repository_description.format(None) {
                ctx.insert("GIT_TAG", &git_tag);
            }
        }
    } else {
        ctx.insert("GIT_TAG", "");
        ctx.insert("GIT_COMMIT", "");
    }

    // Render the Tera template
    let output = match tera.render("index.html", &ctx) {
        Ok(output) => output,
        Err(error) => {
            eprintln!("{:?}", error);
            std::process::exit(1);
        }
    };

    // Write the output to a file
    if let Err(error) = fs::write(&output_file, output) {
        eprintln!("{:?}: {}", &output_file, error);
        std::process::exit(1);
    }

    std::process::exit(0);
}
